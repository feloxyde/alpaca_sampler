from PyQt5.QtGui import QColor


class UIColors:
    # pale colors
    PaleRed = QColor(229, 52, 52)
    PaleYellow = QColor(240, 255, 79)
    PaleBlue = QColor(31, 122, 140)
    PaleBlack = QColor(46, 46, 46)
    PalePink = QColor(247, 111, 142)
    PalePurple = QColor(155, 71, 120)
    PaleGreen = QColor(82, 211, 65)
    PaleOrange = QColor(237, 141, 56)
    PaleWhite = QColor(237, 237, 237)
    PaleGray = QColor(180, 180, 180)

    FlashyPink = QColor(255, 58, 150)
    FlashyBlue = QColor(00, 187, 229)
    FlashyGreen = QColor(24, 209, 00)

    ElectricGreen = QColor(00, 255, 89)
    ElectricBlue = QColor(7, 255, 255)
    ElectricPink = QColor(255, 71, 166)

    SolidYellow = QColor(255, 235, 00)
    PowerYellow = QColor(255, 255, 45)
    PowerGreen = QColor(178, 255, 45)


class UIPalette:
    background = UIColors.PaleBlack
    separator = UIColors.ElectricBlue
    waveform = UIColors.ElectricGreen
    slices = UIColors.ElectricPink
    sliderRail = UIColors.ElectricGreen
    sliderCarriage = UIColors.ElectricPink
    button = UIColors.PaleWhite
    play = UIColors.PaleGreen
    stop = UIColors.PaleRed
    record = UIColors.PaleRed
    compButton = UIColors.PowerYellow
    goComp = compButton
    micButton = UIColors.PaleYellow
    goMic = micButton
    save = UIColors.FlashyGreen
    erase = UIColors.PaleRed


def applyPalette1(black, waveform, erase, slices, separator, color5, color6, color7):
    UIPalette.background = black
    UIPalette.separator = separator
    UIPalette.waveform = QColor(237, 237, 237)
    UIPalette.slices = slices
    UIPalette.sliderRail = waveform
    UIPalette.sliderCarriage = slices
    UIPalette.button = color5
    UIPalette.play = slices
    UIPalette.stop = waveform
    UIPalette.record = QColor(206, 0, 51)
    UIPalette.compButton = UIPalette.button
    UIPalette.goComp = color7
    UIPalette.micButton = UIPalette.button
    UIPalette.goMic = color6
    UIPalette.save = slices
    UIPalette.erase = erase


applyPalette1(UIColors.PaleBlack, UIColors.ElectricPink, UIColors.PaleRed, UIColors.ElectricGreen, UIColors.PaleWhite,
              UIColors.PaleWhite, UIColors.ElectricBlue, UIColors.ElectricGreen)
