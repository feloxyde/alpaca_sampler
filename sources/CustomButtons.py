#!/usr/bin/python3
# -*- coding: utf-8 -*-

from copy import copy

from PyQt5.QtCore import Qt, QPointF
from PyQt5.QtGui import QPainter, QPainterPath, QPen
from PyQt5.QtWidgets import QWidget, QApplication

from Colors import *


class ButtonParams:
    def __init__(self):
        self.record_color = QColor(255, 0, 0)
        self.play_color = QColor(187, 0, 0)

        self.shapeBorderColor = UIPalette.button
        self.shapeBorderColorActive = UIPalette.button
        self.shapeColor = QColor(130, 200, 50)
        self.shapeColorActive = QColor(130, 200, 50)
        self.isShapeFilled = False
        self.isShapeFilledActive = False
        self.iconColor = self.record_color
        self.iconColorActive = self.play_color
        self.iconBorderColor = self.record_color
        self.iconBorderColorActive = self.play_color
        self.isIconFilled = True
        self.shapeBorderThickness = 0.05
        self.iconBorderThickness = 0.05
        self.shapeSize = 0.8
        self.iconSize = 0.4
        self.shapeBorderRadius = 0.1
        self.iconBorderRadius = 0.1
        self.enableAntiAliasing = True
        self.isActive = False


class CustomButton(QWidget):
    def __init__(self, parent, params, shapePainterPathCreator, iconPainterPathCreator, mousePressCallback):
        super().__init__(parent)
        self.shapePainterPathCreator = shapePainterPathCreator
        self.iconPainterPathCreator = iconPainterPathCreator
        self.shapePainterPath = None
        self.iconPainterPath = None
        self.params = copy(params)
        self.initUI()
        self.mousePressCallback = mousePressCallback

    def initUI(self):
        pass

    def setActive(self):
        self.params.isActive = True
        self.update()

    def setInactive(self):
        self.params.isActive = False
        self.update()

    def paintEvent(self, event):
        self.defineShapePainterPath()
        self.defineIconPainterPath()

        width = self.width()
        height = self.height()

        qp = QPainter()
        qp.begin(self)
        if self.params.enableAntiAliasing:
            qp.setRenderHint(QPainter.Antialiasing)

        # drawing shape
        if self.shapePainterPath is not None:
            shapeBorderThickness = self.params.shapeBorderThickness * min(width, height)
            if self.params.isActive:
                qp.setPen(QPen(self.params.shapeBorderColorActive, shapeBorderThickness))
            else:
                qp.setPen(QPen(self.params.shapeBorderColor, shapeBorderThickness))

            if self.params.isShapeFilled:
                if self.params.isActive:
                    qp.setBrush(self.params.shapeColorActive)
                else:
                    qp.setBrush(self.params.shapeColor)

            qp.drawPath(self.shapePainterPath)

        # drawing icon
        if self.iconPainterPath is not None:
            iconBorderThickness = self.params.iconBorderThickness * min(width, height)
            if self.params.isActive:
                qp.setPen(QPen(self.params.iconBorderColorActive, iconBorderThickness, join=Qt.RoundJoin))
            else:
                qp.setPen(QPen(self.params.iconBorderColor, iconBorderThickness, join=Qt.RoundJoin))

            if self.params.isIconFilled:
                if self.params.isActive:
                    qp.setBrush(self.params.iconColorActive)
                else:
                    qp.setBrush(self.params.iconColor)
            qp.drawPath(self.iconPainterPath)

        qp.end()

    def defineShapePainterPath(self):
        if self.shapePainterPathCreator is not None:
            self.shapePainterPath = self.shapePainterPathCreator(self, self.params.shapeSize,
                                                                 self.params.shapeBorderThickness,
                                                                 self.params.shapeBorderRadius)

    def defineIconPainterPath(self):
        if self.iconPainterPathCreator is not None:
            self.iconPainterPath = self.iconPainterPathCreator(self, self.params.iconSize,
                                                               self.params.iconBorderThickness,
                                                               self.params.iconBorderRadius)

    def mousePressEvent(self, event):
        if self.shapePainterPath is not None:
            if self.shapePainterPath.contains(event.pos()):
                self.mousePressCallback()
        else:
            if self.iconPainterPath.contains(event.pos()):
                self.mousePressCallback()


def SquarePainterPathCreator(button, size, borderThickness, borderRadius):
    width = button.width()
    height = button.height()

    outerSize = size * min(width, height)

    borderOverflow = borderThickness * min(width, height)

    outerSize = outerSize - borderOverflow

    oX = (width - outerSize) / 2
    oY = (height - outerSize) / 2

    radius = borderRadius * min(width, height)

    painterPath = QPainterPath()
    painterPath.addRoundedRect(oX, oY, outerSize, outerSize, radius, radius)

    return painterPath


def CirclePainterPathCreator(button, size, borderThickness, borderRadius):
    width = button.width()
    height = button.height()

    outerSize = size * min(width, height)

    borderOverflow = borderThickness * min(width, height)

    outerSize = outerSize - borderOverflow

    oX = (width - outerSize) / 2
    oY = (height - outerSize) / 2

    radius = borderRadius * min(width, height)

    painterPath = QPainterPath()
    painterPath.addEllipse(oX, oY, outerSize, outerSize)
    return painterPath


def TriangleCircleCenteredPainterPathCreator(button, size, borderThickness, borderRadius):
    width = button.width()
    height = button.height()

    outerSize = size * min(width, height)

    borderOverflow = borderThickness * min(width, height)

    outerSize = outerSize - borderOverflow

    oX = (width - outerSize) / 2
    oY = (height - outerSize) / 2

    radius = borderRadius * min(width, height)

    # creating the "real" triangle
    outerABX = width / 2 - outerSize / 3
    outerAY = height / 2 - outerSize / 2
    outerBY = height / 2 + outerSize / 2
    outerCX = width / 2 + outerSize * 2 / 3 - borderOverflow / 2
    outerCY = height / 2

    painterPath = QPainterPath(QPointF(outerABX, outerAY))
    painterPath.lineTo(outerABX, outerBY)
    painterPath.lineTo(outerCX, outerCY)
    painterPath.closeSubpath()

    # painterPath.arcTo(oX, oY, outerSize, outerSize, 60, 60)
    # painterPath.closeSubpath()
    return painterPath


def TriangleSquareCenteredPainterPathCreator(button, size, borderThickness, borderRadius):
    width = button.width()
    height = button.height()

    outerSize = size * min(width, height)

    borderOverflow = borderThickness * min(width, height)

    outerSize = outerSize - borderOverflow

    oX = (width - outerSize) / 2
    oY = (height - outerSize) / 2

    radius = borderRadius * min(width, height)

    # creating the "real" triangle
    outerABX = width / 2 - outerSize / 2
    outerAY = height / 2 - outerSize / 2
    outerBY = height / 2 + outerSize / 2
    outerCX = width / 2 + outerSize / 2
    outerCY = height / 2

    painterPath = QPainterPath(QPointF(outerABX, outerAY))
    painterPath.lineTo(outerABX, outerBY)
    painterPath.lineTo(outerCX, outerCY)
    painterPath.closeSubpath()

    # painterPath.arcTo(oX, oY, outerSize, outerSize, 60, 60)
    # painterPath.closeSubpath()
    return painterPath


class SquareSquareButton(CustomButton):

    def __init__(self, parent, params, mousePressCallback):
        super().__init__(parent, params, SquarePainterPathCreator, SquarePainterPathCreator, mousePressCallback)


class SquareCircleButton(CustomButton):
    def __init__(self, parent, params, mousePressCallback):
        super().__init__(parent, params, SquarePainterPathCreator, CirclePainterPathCreator, mousePressCallback)


class CircleTriangleButton(CustomButton):
    def __init__(self, parent, params, mousePressCallback):
        super().__init__(parent, params, CirclePainterPathCreator, TriangleCircleCenteredPainterPathCreator,
                         mousePressCallback)


class SquareTriangleButton(CustomButton):
    def __init__(self, parent, params, mousePressCallback):
        super().__init__(parent, params, SquarePainterPathCreator, TriangleSquareCenteredPainterPathCreator,
                         mousePressCallback)


class CircleCircleButton(CustomButton):
    def __init__(self, parent, params, mousePressCallback):
        super().__init__(parent, params, CirclePainterPathCreator, CirclePainterPathCreator, mousePressCallback)
