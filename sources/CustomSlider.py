#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter, QColor, QFont, QPainterPath, QPen
from PyQt5.QtCore import Qt, QRectF, QPointF
from Colors import *


class SliderParams:
    def __init__(self):
        self.railLength = 0.9
        self.railThickness = 0.025
        self.railColor = UIPalette.sliderRail
        self.railRadius = 0.005
        self.carriageWidth = 0.3
        self.carriageThickness = 0.06
        self.carriageColor = UIPalette.sliderCarriage
        self.carriageRadius = 0.005
        self.enableAntiAliasing = True
        self.enableRailInstantTravel = True
        self.enableDragAnywhere = True
        self.carriageInitialPosition = 0.5


class VerticalSlider(QWidget):
    def __init__(self, parent, params, onChangeCallback):
        super().__init__(parent)
        self.params = params
        self.sliderCarriagePosition = params.carriageInitialPosition  # ratio of the slider position [0;1]
        self.isDragModeOn = False  # wether the slider can be dragged or not (click on the sliderCarriage)
        self.sliderRailPainterPath = None
        self.sliderCarriagePainterPath = None
        self.onChangeCallback = onChangeCallback  # should take event and slider new position as parameters callback(event, newpos) the new pos is in ratio of progression
        self.lastMouseEventPos = None  # used to remember for the drag
        self.initUI()

    def initUI(self):
        pass

    def moveCarriageTo(self, position):
        self.sliderCarriagePosition = position
        self.update()

    def defineSliderRailPainterPath(self):
        width = self.width()
        height = self.height()

        railThickness = width * self.params.railThickness
        sliderRailHeight = height * self.params.railLength

        oX = width / 2 - railThickness / 2
        oY = height / 2 - sliderRailHeight / 2

        radius = self.params.railRadius * min(width, height)

        painterPath = QPainterPath()
        painterPath.addRoundedRect(oX, oY, railThickness, sliderRailHeight, radius, radius)

        self.sliderRailPainterPath = painterPath

    def defineSliderCarriagePainterPath(self):
        width = self.width()
        height = self.height()

        carriageWidth = width * self.params.carriageWidth
        carriageThickness = width * self.params.carriageThickness
        sliderCarriagePos = height / 2 + (height * (0.5 - self.sliderCarriagePosition) * self.params.railLength)

        oX = width / 2 - carriageWidth / 2
        oY = sliderCarriagePos - carriageThickness / 2

        radius = self.params.carriageRadius * min(width, height)
        painterPath = QPainterPath()
        painterPath.addRoundedRect(oX, oY, carriageWidth, carriageThickness, radius, radius)

        self.sliderCarriagePainterPath = painterPath

    def paintEvent(self, event):
        self.defineSliderRailPainterPath()
        self.defineSliderCarriagePainterPath()

        qp = QPainter()
        qp.begin(self)

        if self.params.enableAntiAliasing:
            qp.setRenderHint(QPainter.Antialiasing)

        # drawing shape
        qp.setPen(QPen(self.params.railColor, 1))
        qp.setBrush(self.params.railColor)
        qp.drawPath(self.sliderRailPainterPath)

        qp.setPen(QPen(self.params.carriageColor, 1))
        qp.setBrush(self.params.carriageColor)
        qp.drawPath(self.sliderCarriagePainterPath)

    def mousePressEvent(self, event):
        # if in the carriage, enter drag mode, if not but in rail range, just move slider
        isCarriageContained = self.sliderCarriagePainterPath.contains(event.pos())
        if self.params.enableRailInstantTravel and self.sliderRailPainterPath.contains(
                event.pos()) and not isCarriageContained:
            # compute position
            minPixPosRail = self.height() / 2 - self.params.railLength * self.height() / 2
            maxPixPosRail = self.height() / 2 + self.params.railLength * self.height() / 2
            pressRailPos = 1.0 - ((event.pos().y() - minPixPosRail) / (maxPixPosRail - minPixPosRail))
            self.isDragModeOn = True
            self.lastMouseEventPos = event.pos()
            self.moveCarriageTo(pressRailPos)
            self.onChangeCallback(pressRailPos)

        if self.params.enableDragAnywhere or isCarriageContained:
            self.isDragModeOn = True
            self.lastMouseEventPos = event.pos()

    def mouseMoveEvent(self, event):
        if self.isDragModeOn:
            height = self.height()
            railHeight = self.params.railLength * height
            pixMove = event.pos().y() - self.lastMouseEventPos.y()
            ratioMove = pixMove / railHeight

            newpos = self.sliderCarriagePosition - ratioMove

            if newpos > 1:
                newpos = 1
            if newpos < 0:
                newpos = 0

            self.lastMouseEventPos = event.pos()
            self.moveCarriageTo(newpos)
            self.onChangeCallback(newpos)

    def mouseReleaseEvent(self, event):
        self.isDragModeOn = False


class HorizontalSlider(QWidget):
    def __init__(self, parent, params, onChangeCallback):
        super().__init__(parent)
        self.params = params
        self.sliderCarriagePosition = params.carriageInitialPosition  # ratio of the slider position [0;1]
        self.isDragModeOn = False  # wether the slider can be dragged or not (click on the sliderCarriage)
        self.sliderRailPainterPath = None
        self.sliderCarriagePainterPath = None
        self.onChangeCallback = onChangeCallback  # should take event and slider new position as parameters callback(event, newpos) the new pos is in ratio of progression
        self.lastMouseEventPos = None  # used to remember for the drag
        self.initUI()

    def initUI(self):
        pass

    def moveCarriageTo(self, position):
        self.sliderCarriagePosition = position
        self.update()

    def defineSliderRailPainterPath(self):
        width = self.width()
        height = self.height()

        railThickness = height * self.params.railThickness
        sliderRailWidth = width * self.params.railLength

        oX = width / 2 - sliderRailWidth / 2
        oY = height / 2 - railThickness / 2

        radius = self.params.railRadius * min(width, height)

        painterPath = QPainterPath()
        painterPath.addRoundedRect(oX, oY, sliderRailWidth, railThickness, radius, radius)

        self.sliderRailPainterPath = painterPath

    def defineSliderCarriagePainterPath(self):
        width = self.width()
        height = self.height()

        carriageHeight = height * self.params.carriageWidth
        carriageThickness = height * self.params.carriageThickness
        sliderCarriagePos = width / 2 + (width * (0.5 - self.sliderCarriagePosition) * self.params.railLength)

        oY = height / 2 - carriageHeight / 2
        oX = sliderCarriagePos - carriageThickness / 2

        radius = self.params.carriageRadius * min(width, height)
        painterPath = QPainterPath()
        painterPath.addRoundedRect(oX, oY, carriageThickness, carriageHeight, radius, radius)

        self.sliderCarriagePainterPath = painterPath

    def paintEvent(self, event):
        self.defineSliderRailPainterPath()
        self.defineSliderCarriagePainterPath()

        qp = QPainter()
        qp.begin(self)

        if self.params.enableAntiAliasing:
            qp.setRenderHint(QPainter.Antialiasing)

        # drawing shape
        qp.setPen(QPen(self.params.railColor, 1))
        qp.setBrush(self.params.railColor)
        qp.drawPath(self.sliderRailPainterPath)

        qp.setPen(QPen(self.params.carriageColor, 1))
        qp.setBrush(self.params.carriageColor)
        qp.drawPath(self.sliderCarriagePainterPath)

    def mousePressEvent(self, event):
        # if in the carriage, enter drag mode, if not but in rail range, just move slider
        isCarriageContained = self.sliderCarriagePainterPath.contains(event.pos())
        if self.params.enableRailInstantTravel and self.sliderRailPainterPath.contains(
                event.pos()) and not isCarriageContained:
            # compute position
            minPixPosRail = self.width() / 2 - self.params.railLength * self.width() / 2
            maxPixPosRail = self.width() / 2 + self.params.railLength * self.width() / 2
            pressRailPos = 1.0 - ((event.pos().x() - minPixPosRail) / (maxPixPosRail - minPixPosRail))
            self.isDragModeOn = True
            self.lastMouseEventPos = event.pos()
            self.moveCarriageTo(pressRailPos)
            self.onChangeCallback(pressRailPos)

        if self.params.enableDragAnywhere or isCarriageContained:
            self.isDragModeOn = True
            self.lastMouseEventPos = event.pos()

    def mouseMoveEvent(self, event):
        if self.isDragModeOn:
            width = self.width()
            railWidth = self.params.railLength * width
            pixMove = event.pos().x() - self.lastMouseEventPos.x()
            ratioMove = pixMove / railWidth

            newpos = self.sliderCarriagePosition - ratioMove

            if newpos > 1:
                newpos = 1
            if newpos < 0:
                newpos = 0

            self.lastMouseEventPos = event.pos()
            self.moveCarriageTo(newpos)
            self.onChangeCallback(newpos)

    def mouseReleaseEvent(self, event):
        self.isDragModeOn = False
