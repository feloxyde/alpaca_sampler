import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

import test_utils as tu
from CustomSlider import *
import unittest

cursorChanges = []

app = QApplication(sys.argv)


def onSliderChange(newPos):
    cursorChanges.append(newPos)


class TestCustomSlider(unittest.TestCase):

    def test_move_cursor(self):
        params = SliderParams()
        params.carriageInitialPosition = 0.0

        slider = VerticalSlider(None, params, onSliderChange)
        self.assertEqual(slider.sliderCarriagePosition, 0.0)
        slider.defineSliderCarriagePainterPath()
        slider.defineSliderRailPainterPath()
        tu.clickAt(slider, 1 / 2, 1 / 2)
        self.assertEqual(slider.sliderCarriagePosition, 1 / 2)
        self.assertEqual(cursorChanges[0], 1 / 2)
        tu.clickAt(slider, 1 / 2, (1 - params.railLength / 2) / 2)
        self.assertEqual(slider.sliderCarriagePosition, 0.75)
        self.assertEqual(cursorChanges[1], 0.75)


if __name__ == '__main__':
    unittest.main()
