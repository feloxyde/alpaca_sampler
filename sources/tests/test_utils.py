from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtTest import QTest


def clickAt(widget, ratioX, ratioY):
    QTest.mouseClick(widget, Qt.LeftButton, pos=QPoint(widget.width() * ratioX, widget.height() * ratioY))
