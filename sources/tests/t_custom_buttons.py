import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))

import test_utils as tu
from CustomButtons import *
import unittest

app = QApplication(sys.argv)
clicks = 0


def clickCallback():
    global clicks
    clicks = clicks + 1


class TestCustomButtons(unittest.TestCase):

    def test_click_in(self):
        global clicks
        clicks = 0
        params = ButtonParams()

        button = SquareSquareButton(None, params, clickCallback)
        button.defineShapePainterPath()
        button.defineIconPainterPath()
        tu.clickAt(button, 1 / 2, 1 / 2)
        self.assertEqual(clicks, 1)


if __name__ == '__main__':
    unittest.main()
