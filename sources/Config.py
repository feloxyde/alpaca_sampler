#This file is a config file for the sampler, providion a default config and loading a file if it exists
#it will create config file if not existing

import CustomButtons as btn
import CustomSlider as sld
from PyQt5.QtGui import QColor
import os
from pathlib import Path
import json

config = {
    #color setup
    "recordButtonIconColor":"#E21818",
    "recordButtonBorderColor":"#FFFFFF",
    "recordButtonIconColor_active":"#FF1C1C",
    "recordButtonBorderColor_active":"#FFFFFF",
    "sliderCarriageColor":"#FFFFFF",
    "sliderRailColor":"#BFBFBF",
    "backgroundColor":"#000000",
    "waveformColor":"#BFBFBF",
    "waveformColor_active":"#FFFFFF",
    "sliceColor":"#FFFFFF",

    #screen layout setup
    "waveformPanelWeight":50,
    "controlPanelWeight":50,
    "waveformControlSpacerWeight":10,
    "topControlSpacerWeight":10,
    "bottomWaveformSpacerWeight":10,

    #control panel layout setup
    "controlPanelLeftSpacerWeight":10,
    "controlPanelRightSpacerWeight":10,
    "controlPanelCenterSpacerWeight":10,
    "controlPanelRecordButtonWeight":10,
    "controlPanelSliderWeight":50,
    

    #record button setup
    "recordButtonShapeSize":0.8,
    "recordButtonIconSize":0.4,
    "recordButtonBorderThickness":0.05,
    
    #slider setup
    "sliderRailThickness":0.025,
    "sliderRailLength":0.9,
    "sliderCarriageThickness":0.06,
    "sliderCarriageWidth":0.3
}

def getRecordButtonParams():
    recordParams = btn.ButtonParams()
    recordParams.shapeBorderColor = QColor(config["recordButtonBorderColor"])
    recordParams.shapeBorderColorActive = QColor(config["recordButtonBorderColor_active"])
    #recordParams.shapeColor = QColor(config["backgroundColor"])
    #recordParams.shapeColorActive = QColor(config["backgroundColor"])
    recordParams.iconColor = QColor(config["recordButtonIconColor"])
    recordParams.iconColorActive = QColor(config["recordButtonIconColor_active"])
    recordParams.iconBorderColor = QColor(config["recordButtonIconColor"])
    recordParams.iconBorderColorActive = QColor(config["recordButtonIconColor_active"])
    recordParams.shapeBorderThickness = config["recordButtonBorderThickness"]
    recordParams.shapeSize = config["recordButtonShapeSize"]
    recordParams.iconSize = config["recordButtonIconSize"]
    return recordParams

def getSliderParams():
    sliderParams = sld.SliderParams()
    sliderParams.railLength = config["sliderRailLength"]
    sliderParams.railThickness = config["sliderRailThickness"]
    sliderParams.railColor = QColor(config["sliderRailColor"])
    sliderParams.carriageWidth = config["sliderCarriageWidth"]
    sliderParams.carriageThickness = config["sliderCarriageThickness"]
    sliderParams.carriageColor = QColor(config["sliderCarriageColor"])
    return sliderParams


def loadConfig():
    currentDir = os.getcwd()
    #if the file exists we read it else we create it
    config_file = Path(currentDir+"/config.json")
    if(config_file.is_file()):
        with open(config_file, "r") as read_file:
            global config
            config = json.load(read_file)
    else:
        with open(config_file, "w") as write_file:
            json.dump(config, write_file, indent=1, separators=(",", ": "))